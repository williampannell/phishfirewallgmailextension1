class CreateEmails < ActiveRecord::Migration[6.1]
    def change
      create_table :emails do |t|
        t.string :content
        t.string :date_sent
        t.string :sender
  
        t.timestamps
      end
    end
  end