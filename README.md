PhishFirewall Gmail Extension

In order to use:
1. Download this repository 
2. Go to your Google Chrome extensions and upload the folder via the "load unpacked" button
3. Open folder in a code editor
4. cd BackendTest
5. cd email_log_api
6. Run rails s in your terminal to run it locally
7. Go to gmail, view an email, click reply to bring up a compose view, click the "forward to PhishFirewall" button. The email data will be sent to the locally run API.