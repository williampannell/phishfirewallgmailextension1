(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

// loader-code: wait until gmailjs has finished loading, before triggering actual extensiode-code.
const loaderId = setInterval(() => {
    if (!window._gmailjs) {
        return;
    }

    clearInterval(loaderId);
    startExtension(window._gmailjs);
}, 100);

// actual extension-code
function startExtension(gmail) {
    window.gmail = gmail;
    
    gmail.observe.on("load", () => {
        var dataToSend = "";

        gmail.observe.on("view_email", (domEmail) => {
            console.log("Looking at email:", domEmail);
            const emailData = gmail.new.get.email_data(domEmail);
            dataToSend = emailData;
            console.log("Email data:", emailData);
        });

        gmail.observe.on("compose", (compose) => {
            var compose_ref = gmail.dom.composes()[0];
            gmail.tools.add_compose_button(compose_ref, 'Forward to PhishFirewall', function() {
                //console.log("Email Text:",dataToSend.content_html.replace(/<[^>]*>?/gm, ''), "Email Sender:", dataToSend.from, "Email Date:", dataToSend.date);
                fetch("http://[::1]:3000/emails", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        Accept: "application/json"
                    },
                    body: JSON.stringify({"email": {
                        "content": dataToSend.content_html.replace(/<[^>]*>?/gm, '').replace("\r\n", ''),
                        "date_sent": dataToSend.date,
                        "sender": dataToSend.from.address
                    }})
                })
            }, 'Custom Style Classes'); 
        });
    });
}


},{}]},{},[1]);
